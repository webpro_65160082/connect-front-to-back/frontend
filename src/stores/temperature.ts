import { ref } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from '@/stores/loading';
import temperature from '@/services/temperature';

export const useTemperatureStore = defineStore('temperature', () => {
  const valid = ref(false)
  const celsius = ref(0)
  const result = ref(0)
  const loadingStore = useLoadingStore()

  // function convert(celsius: number): number {
  //   return (celsius * 9.0) / 5 + 32
  // }

  async function callConvert() {
    // result.value = convert(celsius.value)
    loadingStore.doLoad()
    try {

      result.value = await temperature.convert(celsius.value)
    } catch (e) {
      console.log(e);
    }
    loadingStore.finish()
  }
  return { valid, result, celsius, callConvert }
})
