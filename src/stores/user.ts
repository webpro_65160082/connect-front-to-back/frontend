import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import userService from '@/services/userService'
import type { User } from '@/types/User'

export const useUSerStore = defineStore('user', () => {
  const loadingStore = useLoadingStore()
  const users = ref<User[]>([])
  //data function
  async function getUsers() {
    loadingStore.doLoad()
    const res = await userService.getUsers()
    users.value = res.data
    loadingStore.finish()
  }
  async function getUser(id: number) {
    loadingStore.doLoad()
    const res = await userService.getUser(id)
    users.value = res.data
    loadingStore.finish()
  }

  async function saveUser(user: User) {
    loadingStore.doLoad()
    if (user.id < 0) {
      //Add
      const res = await userService.addUser(user)
    } else {
      //Update
      const res = await userService.updateUser(user)
    }
    await getUsers()
    loadingStore.finish()
  }

  async function deleteUser(user: User) {
    loadingStore.doLoad()

    const res = await userService.deleteUser(user)
    await getUsers()
    loadingStore.finish()
  }

  return { users, getUser, getUsers, saveUser, deleteUser }
})
